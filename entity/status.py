from enum import Enum

class Status(Enum):
    TODO = 1
    INPROGRES = 2
    FINISHED = 3