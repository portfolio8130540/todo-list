import entity.priority as Priority 
import entity.status as Status
import json

class Task():
    id:int
    title:str
    dsc:str
    priority:Priority
    status:Status

    def __init__(self,id:int,title:str,dsc:str,priority:Priority,status:Status):
        self.id=id
        self.title=title
        self.dsc=dsc
        self.priority=priority
        self.status=status

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)

    def to_tuple(self):
        return (self.title,self.dsc,self.priority,self.status)

    @staticmethod
    def from_json(dct:dict):
        return Task(dct.get('id'),dct.get('title'),dct.get('dsc'),dct.get('priority'),dct.get('status'))
