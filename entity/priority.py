from enum import Enum

class Priority(Enum):
    CRITICAL = 1
    MEDIUM = 2
    LOW = 3