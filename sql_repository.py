import sqlite3
from entity.task import Task

class DatabaseRepository:
    __instance = None
    __conn:sqlite3.Connection
    __cur:sqlite3.Cursor

    '''The implementation of a singleton in this case doesn't make sense as the session is not stored in an object instance. It was only done for testing purposes.'''
    @classmethod
    def get_instance(cls):
        if cls.__instance == None:
            cls.__instance = cls.__new__(cls)
            cls.__init_database(cls.__instance)
        return cls.__instance

    def __init__(self):
        raise RuntimeError("This is a Singleton, invoke get_instance() insted.")

    def __open_connection(self):
        self.__conn = sqlite3.connect('todolist.db')
        self.__cur = self.__conn.cursor()

    def __close_connection(self):
        self.__cur.close()
        self.__conn.close()

    def __init_database(self):
        self.__open_connection()
        self.__cur.execute("CREATE TABLE IF NOT EXISTS todo(id integer primary key autoincrement,title TEXT,dsc TEXT,priority TEXT,status TEXT)")
        self.__conn.commit()
        self.__close_connection()

    def create_task(self,task:Task):
        self.__open_connection()
        self.__cur.execute("INSERT INTO todo(title,dsc,priority,status) values(?,?,?,?)",task.to_tuple())
        self.__conn.commit()
        id = self.__cur.lastrowid
        self.__close_connection()
        return id

    def get_all_tasks(self):
        self.__open_connection()
        self.__cur.execute("SELECT * FROM todo")
        tasks_list:[Task] = []
        for id, title, dsc, priority, status in self.__cur:
            tasks_list.append(
                Task(id,title,dsc,priority,status)
            )
        self.__close_connection()
        return tasks_list

    def update_task_status(self,id:int,status:int):
        self.__open_connection()
        try:
            self.__cur.execute("UPDATE todo SET status=? WHERE id=?",(status,id))
            self.__conn.commit()
            self.__cur.execute("SELECT * FROM todo WHERE id=?",id)
            self.__conn.commit()
            for id, title, dsc, priority, status in self.__cur:
                task:Task = Task(id,title,dsc,priority,status)
        except Exception as e:
            self.__close_connection()
            print(e)
            raise e
        self.__close_connection()
        return task

    def remove_task(self,id):
        self.__open_connection()
        try:
            self.__cur.execute("UPDATE todo SET status=4 WHERE id=?",id)
            self.__conn.commit()
        except Exception as e:
            self.__close_connection()
            raise e
        self.__close_connection()
