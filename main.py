from flask import Flask, request, jsonify
import json
from jsonschema import ValidationError
import request_validator as rval
from sql_repository import DatabaseRepository
from entity.task import Task

app = Flask(__name__)
sqlr = DatabaseRepository.get_instance()


@app.route('/',methods=['GET'])
def home():
    tasks:[Task] = sqlr.get_all_tasks()
    return json.dumps([obj.__dict__ for obj in tasks])



@app.route('/', methods=['POST'])
def create_task():
    try:
        body = rval.create_task_validator()
        id = sqlr.create_task(body)
        return {'id':id}
    except ValidationError as e:
         return jsonify({'error': str(e.message)}), 400



@app.route('/',methods=['DELETE'])
def remove_task():
    id = request.args.get('id')
    if id is None:
        return {'message':'Id parameter is missing'},400
    try:
        sqlr.remove_task(id)
        return {'message':'Succesful delete task'}
    except Exception as e:
        return {'message':"Task don't exist"},400
    


@app.route('/',methods=['PATCH'])
def update_task_status():
    id = request.args.get('id')
    status = request.args.get('status')
    if id is None:
        return {'message':'Id parameter is missing'},400
    try:
        task = sqlr.update_task_status(id,status)
        return task.to_json()
    except Exception as e:
        print(e)
        return {'message':"Task don't exist"},400
    


if __name__ == '__main__':
    app.run()