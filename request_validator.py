from jsonschema import validate, ValidationError
from flask import request
import json
from entity.task import Task
from entity.priority import Priority

def create_task_validator():
    with open('./schema/create_task_schema.json') as data:
        schema = json.load(data)
        data.close() 

    data_as_str = request.data.decode('utf-8')
    data_as_dict = json.loads(data_as_str)

    
    try:
        validate(data_as_dict, schema)
        task:Task = Task.from_json(data_as_dict)
    except ValidationError as e:
        raise e
    
    return task